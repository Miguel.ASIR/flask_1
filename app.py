from flask import Flask,render_template
app = Flask(__name__)

@app.route('/')
def inicio():
    return render_template("inicio.html")

@app.route('/potencia/<int:base>/<int:exponente>',methods=["GET","POST"])
def potencia(base,exponente):
    if exponente > 0:
       return render_template("potencia.html",resultado=base**exponente,base=base,expo=exponente)
    if exponente == 0:
        return render_template("potencia.html",resultado=1,base=base,expo=exponente)
    if exponente < 0:
        return render_template("potencia.html",resultado=1/(base**exponente),base=base,expo=exponente)

@app.route('/cuenta/<palabra>/<letra>',methods=["GET","POST"])
def letra_palabras(letra,palabra):
    try:
        if len(letra)==1 and palabra.count(letra)>=1:
            return render_template("palabras.html",palabra=palabra,letra=letra,veces=palabra.count(letra))
    except TypeError:
        return "404 Not Found"
    else:
        return "404 Not Found"
        
@app.route('/libro/<codigo>',methods=["GET","POST"])
def libreria(codigo):
    from lxml import etree
    doc = etree.parse('libros.xml')
    cod=codigo
    try:
        if cod in doc.xpath('//codigo/text()'):
            autores=doc.xpath('//libro[codigo="%s"]/autor/text()'%cod)
            titulos=doc.xpath('//libro[codigo="%s"]/titulo/text()'%cod)
            for autor,titulo in zip (autores,titulos):
                return render_template("librerias.html",autor=autor,titulo=titulo)
    except TypeError:
        return "404 Not Found"
    else:
        return "404 Not Found"

app.run(debug=True)